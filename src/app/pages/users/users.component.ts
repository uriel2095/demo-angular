import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/interface/user';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent {
  users: IUser[] = [];

  constructor(
    private userService: UserService,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers() {
    debugger;

    this.userService.getAllUsers().subscribe(
      (response) => {
        this.users = response;
        this._snackBar.open('Users found');
      },
      (error) => {
        this._snackBar.open('Users notfound: ', error.message);
      }
    );
  }
}
