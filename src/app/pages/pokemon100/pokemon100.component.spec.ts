import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pokemon100Component } from './pokemon100.component';

describe('Pokemon100Component', () => {
  let component: Pokemon100Component;
  let fixture: ComponentFixture<Pokemon100Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pokemon100Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Pokemon100Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
