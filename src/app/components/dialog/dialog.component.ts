import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IPokemonDetail } from 'src/app/interface/pokemon';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent {
  pokemon: IPokemonDetail;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IPokemonDetail,
    public dialogRef: MatDialogRef<DialogComponent>
  ) {
    this.pokemon = data;
  }
}
