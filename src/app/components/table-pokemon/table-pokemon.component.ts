import { Component, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { IPokemon, IPokemonDetail } from 'src/app/interface/pokemon';
import { PokemonService } from 'src/app/service/pokemon.service';
import { DialogComponent } from '../dialog/dialog.component';
import { debounceTime } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-table-pokemon',
  templateUrl: './table-pokemon.component.html',
  styleUrls: ['./table-pokemon.component.scss'],
})
export class TablePokemonComponent {
  pokemons: IPokemon[] = [];
  detailPokemon: IPokemonDetail;
  search = new FormControl();
  searchText: string = '';

  displayedColumns: string[] = ['name', 'url', 'action'];
  dataSource = new MatTableDataSource(this.pokemons);
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    private pokemonService: PokemonService,
    private matDialog: MatDialog
  ) {
    this.detailPokemon = {
      id: 0,
      attack: 0,
      defense: 0,
      hp: 0,
      img: '',
      name: '',
      speed: 0,
    };
  }

  ngOnInit(): void {
    this.getPokemons(150, 0);
    this.search.valueChanges.pipe(debounceTime(200)).subscribe((value) => {
      this.searchText = value!;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  getPokemons(limit: number, offset: number) {
    this.pokemonService.getAllPokemons(limit, offset).subscribe((response) => {
      this.pokemons = response.results;
      this.dataSource.data = this.pokemons;
    });
  }

  getDetailPokemon(urlEnpoint: string) {
    this.pokemonService.getPokemon(urlEnpoint).subscribe((response) => {
      this.detailPokemon.id = response.id;
      this.detailPokemon.name = response.name;
      this.detailPokemon.attack = response.stats[1].base_stat;
      this.detailPokemon.hp = response.stats[0].base_stat;
      this.detailPokemon.defense = response.stats[2].base_stat;
      this.detailPokemon.speed = response.stats[5].base_stat;
      this.detailPokemon.img = response.sprites.other.dream_world.front_default;

      this.openDialog(this.detailPokemon);
    });
  }

  openDialog(pokemon: IPokemonDetail) {
    let dialog = this.matDialog.open(DialogComponent, {
      data: pokemon,
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
