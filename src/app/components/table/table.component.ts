import { Component, Input, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSort } from '@angular/material/sort';
import { debounceTime } from 'rxjs';
import { IUser } from 'src/app/interface/user';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent {
  @Input() users: IUser[] = [];
  search = new FormControl('');
  searchText: string = '';
  @ViewChild(MatSort) sort!: MatSort;

  displayedColumns: string[] = ['avatar', 'id', 'name', 'email'];
  data: IUser[] = [];

  constructor() {}

  ngOnInit(): void {
    this.search.valueChanges.pipe(debounceTime(200)).subscribe((value) => {
      this.searchText = value!;
    });
  }

  ngOnChanges(): void {
    this.data = this.users;
  }
}
