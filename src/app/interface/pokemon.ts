export interface IPokemon {
  name: string;
  url: string;
}

export interface IPokemonDetail {
  id: number;
  name: string;
  hp: number;
  attack: number;
  defense: number;
  speed: number;
  img: string;
}
