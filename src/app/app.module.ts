import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './pages/users/users.component';
import { HttpClientModule } from '@angular/common/http';
import { TableComponent } from './components/table/table.component';

import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { FilterUserPipe } from './pipes/filter-user.pipe';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { Pokemon100Component } from './pages/pokemon100/pokemon100.component';
import { TablePokemonComponent } from './components/table-pokemon/table-pokemon.component';
import { MatButtonModule } from '@angular/material/button';
import { DialogComponent } from './components/dialog/dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FilterPokemonPipe } from './pipes/filter-pokemon.pipe';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    TableComponent,
    FilterUserPipe,
    Pokemon100Component,
    TablePokemonComponent,
    DialogComponent,
    FilterPokemonPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSortModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatButtonModule,
    MatDialogModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
