import { Pipe, PipeTransform } from '@angular/core';
import { IPokemon } from '../interface/pokemon';

@Pipe({
  name: 'filterPokemon',
})
export class FilterPokemonPipe implements PipeTransform {
  transform(list: IPokemon[], text: string): IPokemon[] {
    if (text.length == 0) {
      return list;
    }

    const filteredValue = list.filter((item) =>
      item.name.toLowerCase().includes(text.toLowerCase())
    );
    return filteredValue;
  }
}
