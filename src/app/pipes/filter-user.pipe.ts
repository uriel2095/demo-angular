import { Pipe, PipeTransform } from '@angular/core';
import { IUser } from '../interface/user';

@Pipe({
  name: 'filterUser',
})
export class FilterUserPipe implements PipeTransform {
  transform(list: IUser[], text: string): IUser[] {
    if (text.length == 0) {
      return list;
    }

    const filteredValue = list.filter(
      (item) =>
        item.name.toLowerCase().includes(text.toLowerCase()) ||
        item.email.toLowerCase().includes(text.toLowerCase())
    );
    return filteredValue;
  }
}
