import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { IUser } from '../interface/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  apiUrl: string;
  headers: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.apiUrl = 'http://localhost:3000/users';
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  }

  getAllUsers(): Observable<IUser[]> {
    return this.httpClient.get<IUser[]>(this.apiUrl, { headers: this.headers });
  }
}
