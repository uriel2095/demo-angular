import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  apiUrl: string;
  headers: HttpHeaders;

  constructor(private httpClient: HttpClient) {
    this.apiUrl = 'https://pokeapi.co/api/v2/pokemon';
    this.headers = new HttpHeaders({ 'Content-Type': 'application/json' });
  }

  getAllPokemons(limit: number, offset: number): Observable<any> {
    return this.httpClient.get<any>(
      `${this.apiUrl}?limit=${limit}&offset=${offset}`,
      { headers: this.headers }
    );
  }

  getPokemon(urlEndpoint: string) {
    return this.httpClient.get<any>(urlEndpoint, { headers: this.headers });
  }
}
